#pragma once

#include <discordframework/core/configuration.h>
#include <discordframework/utils/regex.h>
#include <map>
#include <sharedlibs-loader/plugin.h>

class SupportLauncher : public Plugin {
public:
  SupportLauncher(Bot *bot, PluginInfo *info);
  ~SupportLauncher();

  void onLoad();
  void onInteractionCreate(const dpp::interaction_create_t &event);
  void onMessageCreate(const dpp::message_create_t &event);
  void onThreadCreate(const dpp::thread_create_t &event);
  void onAutocomplete(const dpp::autocomplete_t &event);

private:
  dpp::snowflake m_guild_id;
  std::string m_haste_url, m_packs_url;
  std::unique_ptr<Helpers::PCRE> m_charset =
      std::make_unique<Helpers::PCRE>("; charset=");
  std::unique_ptr<DiscordFramework::Configuration> m_config;

  std::string m_htmlTmp = "";
  std::chrono::time_point<std::chrono::system_clock> m_last_check{};
};

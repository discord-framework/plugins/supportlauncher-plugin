#include "gumbo_extended.h"

#include <cstring>
#include <stdint.h>

GumboNode *get_element_by_id(const char *id, GumboNode *node) {
  if (GUMBO_NODE_DOCUMENT != node->type && GUMBO_NODE_ELEMENT != node->type) {
    return nullptr;
  }

  GumboAttribute *node_id =
      gumbo_get_attribute(&node->v.element.attributes, "id");
  if (node_id && 0 == strcmp(id, node_id->value)) {
    return node;
  }

  // iterate all children
  GumboVector *children = &node->v.element.children;
  for (uint64_t i = 0; i < children->length; i++) {
    GumboNode *node =
        get_element_by_id(id, static_cast<GumboNode *>(children->data[i]));
    if (node)
      return node;
  }

  return nullptr;
}

GumboNode *get_next_element_by_tag(GumboTag tag, GumboNode *node) {
  if (node->type != GUMBO_NODE_ELEMENT) {
    return nullptr;
  }
  GumboNode *parent = node->parent;

  GumboVector *children = &parent->v.element.children;
  for (uint64_t i = node->index_within_parent + 1; i < children->length; i++) {
    auto c = static_cast<GumboNode *>(children->data[i]);
    if (c->type != GUMBO_NODE_ELEMENT)
      continue;

    if (c->v.element.tag == tag) {
      return c;
    }
  }

  return nullptr;
}

GumboNode *get_first_child(GumboTag tag, GumboNode *node) {
  if (node->type != GUMBO_NODE_ELEMENT) {
    return nullptr;
  }

  GumboVector *children = &node->v.element.children;
  for (uint64_t i = 0; i < children->length; i++) {
    auto c = static_cast<GumboNode *>(children->data[i]);
    if (c->v.element.tag == tag) {
      return c;
    }
  }

  return nullptr;
}

std::string get_text(GumboNode *node) {
  if (node->type != GUMBO_NODE_ELEMENT) {
    return nullptr;
  }

  GumboVector *children = &node->v.element.children;

  for (uint64_t i = 0; i < children->length; i++) {
    auto c = static_cast<GumboNode *>(children->data[0]);

    if (c->type == GUMBO_NODE_TEXT) {
      std::string txt = c->v.text.text;
      return txt;
    }
  }

  return nullptr;
}

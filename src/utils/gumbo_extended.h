#pragma once

#include <gumbo.h>
#include <string>

GumboNode *get_element_by_id(const char *id, GumboNode *node);
GumboNode *get_next_element_by_tag(GumboTag tag, GumboNode *node);
GumboNode *get_first_child(GumboTag tag, GumboNode *node);
std::string get_text(GumboNode *node);

#include "supportlauncher.h"

#include <discordframework/utils/strings.h>
#include <nlohmann/json-schema.hpp>
#include <thread>

static json plugin_config = R"(
{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "SupportLauncher plugin Configuration",
  "properties": {
    "guild_id": {
      "type": "number"
    },
    "haste_url": {
      "type": "string"
    },
    "packs_url": {
      "type": "string"
    }
  },
  "required": ["guild_id", "haste_url", "packs_url"]
}
)"_json;

SupportLauncher::SupportLauncher(Bot *bot, PluginInfo *info)
    : Plugin(bot, info) {
  m_config = std::make_unique<DiscordFramework::Configuration>(
      this->config_path().string(),
      json{{"guild_id", ""}, {"haste_url", ""}, {"packs_url", ""}});

  try {
    nlohmann::json_schema::json_validator validator;

    validator.set_root_schema(plugin_config);
    validator.validate(*m_config->JSON());

    m_guild_id = m_config->JSON()->at("guild_id").get<dpp::snowflake>();
    m_haste_url = m_config->JSON()->at("haste_url").get<std::string>();
    m_packs_url = m_config->JSON()->at("packs_url").get<std::string>();

    auto h_interaction = m_bot->cluster->on_interaction_create.attach(std::bind(
        &SupportLauncher::onInteractionCreate, this, std::placeholders::_1));

    auto h_message = m_bot->cluster->on_message_create.attach(std::bind(
        &SupportLauncher::onMessageCreate, this, std::placeholders::_1));

    auto h_thread = m_bot->cluster->on_thread_create.attach(std::bind(
        &SupportLauncher::onThreadCreate, this, std::placeholders::_1));

    auto h_autocomplete = m_bot->cluster->on_autocomplete.attach(std::bind(
        &SupportLauncher::onAutocomplete, this, std::placeholders::_1));

    m_handlers.insert({"interaction", h_interaction});
    m_handlers.insert({"message", h_message});
    m_handlers.insert({"thread", h_thread});
    m_handlers.insert({"autocomplete", h_autocomplete});

    this->onLoad();
  } catch (const std::exception &e) {
    m_bot->cluster->log(
        dpp::ll_warning,
        fmt::format("[SupportLauncher] Error while loading config", e.what()));
  }
}

SupportLauncher::~SupportLauncher() {
  if (!m_handlers.empty()) {
    m_bot->cluster->on_integration_create.detach(m_handlers["interaction"]);
    m_bot->cluster->on_message_create.detach(m_handlers["message"]);
    m_bot->cluster->on_thread_create.detach(m_handlers["thread"]);
    m_bot->cluster->on_autocomplete.detach(m_handlers["autocomplete"]);
  }
}

PLUGIN(SupportLauncher);

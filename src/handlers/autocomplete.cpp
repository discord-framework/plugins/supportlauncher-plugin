#include "../supportlauncher.h"

#include "../utils/gumbo_extended.h"
#include <cpr/cpr.h>
#include <discordframework/utils/strings.h>

std::vector<std::string> search_languages(GumboNode *node) {
  std::vector<std::string> list;

  if (node->type != GUMBO_NODE_ELEMENT) {
    return {};
  }

  auto title = get_element_by_id("awesome-learning-dev-", node);

  if (!title)
    return {};

  auto mainUl = get_next_element_by_tag(GUMBO_TAG_UL, title);
  if (!mainUl)
    return {};

  GumboNode *parentList = get_first_child(GUMBO_TAG_LI, mainUl);

  if (!parentList)
    return {};

  GumboNode *ul = get_first_child(GUMBO_TAG_UL, parentList);

  if (!ul)
    return {};

  GumboVector *liChildrens = &ul->v.element.children;

  for (uint64_t i = 0; i < liChildrens->length; i++) {
    auto c = static_cast<GumboNode *>(liChildrens->data[i]);

    auto a = get_first_child(GUMBO_TAG_A, c);
    if (!a)
      continue;

    auto text = get_text(a);

    if (!text.empty()) {
      list.push_back(text);
    }
  }

  return list;
}

std::string fetch_page() {
  cpr::Response r = cpr::Get(cpr::Url{"https://learndev.info/fr"});

  if (r.status_code != 200) {
    return "";
  }

  return r.text;
}

void SupportLauncher::onAutocomplete(const dpp::autocomplete_t &event) {
  for (auto &opt : event.options) {
    if (opt.name != "name" || !opt.focused)
      continue;

    const auto now = std::chrono::system_clock::now();
    if ((now - m_last_check) > std::chrono::hours(24)) {
      m_last_check = now;
      m_htmlTmp = fetch_page();
    }

    dpp::interaction_response response{dpp::ir_autocomplete_reply};

    if (!m_htmlTmp.empty()) {
      GumboOutput *dom = gumbo_parse(m_htmlTmp.c_str());
      auto list = search_languages(dom->root);
      gumbo_destroy_output(&kGumboDefaultOptions, dom);

      std::string language = std::get<std::string>(opt.value);
      language = Helpers::Strings::toLower(language);

      std::vector<std::string> output{};

      std::copy_if(list.begin(), list.end(), std::back_inserter(output),
                   [&language](const std::string &l) {
                     if (language.empty())
                       return true;

                     return Helpers::Strings::toLower(l).find(language) !=
                            std::string::npos;
                   });

      for (auto o : output) {
        response.add_autocomplete_choice(dpp::command_option_choice(o, o));
      }
    }

    m_bot->cluster->interaction_response_create(event.command.id,
                                                event.command.token, response);

    break;
  }
}

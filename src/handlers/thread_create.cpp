#include "../supportlauncher.h"

void SupportLauncher::onThreadCreate(const dpp::thread_create_t &event) {
  m_bot->cluster->current_user_join_thread(event.created.id);
}

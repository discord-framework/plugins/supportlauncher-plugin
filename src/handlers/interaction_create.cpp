#include "../supportlauncher.h"

#include "../commands/about.h"
#include "../commands/ask.h"
#include "../commands/googleit.h"
#include "../commands/haste.h"
#include "../commands/learnjava.h"
#include "../commands/packs.h"
#include "../commands/versions.h"

#include "../commands/learndev.h"

void SupportLauncher::onInteractionCreate(
    const dpp::interaction_create_t &event) {
  if (event.command.type == dpp::it_application_command) {
    dpp::command_interaction cmd_data =
        std::get<dpp::command_interaction>(event.command.data);

    if (cmd_data.name == "haste") {
      haste_response(event, m_bot, m_haste_url);
    } else if (cmd_data.name == "about") {
      about_response(event, m_bot);
    } else if (cmd_data.name == "packs") {
      packs_response(event, m_bot, m_packs_url);
    } else if (cmd_data.name == "googleit") {
      googleit_response(event);
    } else if (cmd_data.name == "learnjava") {
      learnjava_response(event);
    } else if (cmd_data.name == "ask") {
      ask_response(event, m_bot);
    } else if (cmd_data.name == "versions") {
      version_response(event, m_bot);
    } else if (cmd_data.name == "learndev") {
      learndev_response(event, m_bot, m_htmlTmp);
    }
  }
}

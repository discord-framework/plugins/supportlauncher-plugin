#include "../supportlauncher.h"
#include <discordframework/services/logger.h>
#include <discordframework/services/slashcommands.h>

void SupportLauncher::onLoad() {
  std::vector<dpp::slashcommand> commands = {};

  auto commands_mgr =
      this->m_bot->serviceManager()
          ->get<DiscordFramework::Services::SlashCommandsService>(
              "slashcommands");
  auto logger = this->m_bot->serviceManager()
                    ->get<DiscordFramework::Services::LoggerService>("logger")
                    ->instance();

  dpp::slashcommand hastebin_cmd;
  hastebin_cmd.set_name("haste")
      .set_description("Téléverser un fichier sur HasteBin")
      .set_application_id(m_bot->cluster->me.id)
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .add_option(dpp::command_option(dpp::co_user, "mention",
                                      "Utilisateur à mentionner", false));
  commands.push_back(hastebin_cmd);

  dpp::slashcommand about_cmd;
  about_cmd.set_name("about")
      .set_description("Affiche les informations du bot")
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_application_id(m_bot->cluster->me.id);
  commands.push_back(about_cmd);

  dpp::slashcommand packs_cmd;
  packs_cmd.set_name("packs")
      .set_description("Lien vers les differents packs Minecraft")
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_application_id(m_bot->cluster->me.id);
  commands.push_back(packs_cmd);

  dpp::slashcommand google_cmd;
  google_cmd.set_name("googleit")
      .set_application_id(m_bot->cluster->me.id)
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_description("Envoie un GIF");
  commands.push_back(google_cmd);

  dpp::slashcommand learnjava_cmd;
  learnjava_cmd.set_name("learnjava")
      .set_application_id(m_bot->cluster->me.id)
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_description("Envoie un GIF");
  commands.push_back(learnjava_cmd);

  dpp::slashcommand learndev_cmd;
  learndev_cmd.set_name("learndev")
      .set_application_id(m_bot->cluster->me.id)
      .set_description("Trouver des cours dans différent langages")
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .add_option(dpp::command_option(dpp::co_string, "name", "Nom du langage")
                      .set_auto_complete(true));
  commands.push_back(learndev_cmd);

  dpp::slashcommand ask_cmd;
  ask_cmd.set_name("ask")
      .set_application_id(m_bot->cluster->me.id)
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_description("Don't ask to ask");
  commands.push_back(ask_cmd);

  dpp::slashcommand versions_cmd;
  versions_cmd.set_name("versions")
      .set_application_id(m_bot->cluster->me.id)
      .set_type(dpp::slashcommand_contextmenu_type::ctxm_chat_input)
      .set_description("Équivalence des versions");
  commands.push_back(versions_cmd);

  for (auto &cmd : commands) {
    try {
      commands_mgr->register_command(m_guild_id, cmd);
    } catch (std::out_of_range &e) {
      logger->error("Error: {}", e.what());
    }
  }
}

#include "../supportlauncher.h"

#include <discordframework/exceptions.h>
#include <discordframework/utils/files.h>

void SupportLauncher::onMessageCreate(const dpp::message_create_t &event) {
  const dpp::message &msg = event.msg;

  if (msg.author.id == m_bot->getID() || msg.guild_id != m_guild_id) {
    return;
  }

  if (msg.attachments.size() > 0 && msg.content != "--ignore") {
    for (const auto &attach : msg.attachments) {
      if (m_charset->match(attach.content_type)) {
        std::string file, response;

        try {
          file = Helpers::Files::downloadFile(attach.url);

          if (file.length() <= 0) {
            throw DiscordFramework::http_exception("Could not download file");
          }
        } catch (std::exception &e) {
          m_bot->cluster->log(
              dpp::ll_error,
              fmt::format("Error while downloading file: {}", e.what()));
          continue;
        }

        try {
          response = Helpers::Files::uploadFile(
              fmt::format("{}/documents", m_haste_url), file);

          if (response.length() <= 0) {
            throw DiscordFramework::http_exception("Empty upload response");
          }
        } catch (std::exception &e) {
          m_bot->cluster->log(
              dpp::ll_error,
              fmt::format("Error while uploading file: {}", e.what()));
          continue;
        }

        try {
          json content = json::parse(response);

          if (content.contains("key")) {
            std::string link =
                fmt::format("{}/{}", m_haste_url, content["key"]);

            dpp::embed e;
            e.set_title(":white_check_mark: J'ai uploadé ton fichier !");
            e.set_description(
                fmt::format("Par: <@{}>\n{}", msg.author.id, link));
            e.set_color(3447003); // blue

            dpp::embed_footer e_footer;
            e_footer.set_text(fmt::format("Powered by {}@{} - {}",
                                          m_bot->getName(), m_bot->getVersion(),
                                          m_bot->getAuthor()));
            e.set_footer(e_footer);

            dpp::message m{msg.channel_id, e};
            m.set_reference(msg.id);

            m_bot->cluster->message_create(m);
          } else {
            std::string message = content["message"];

            dpp::embed e;
            e.set_title(":warning: Le fichier n'a pas été uploadé");
            e.set_description(
                fmt::format("Par: <@{}>\nErreur: {}", msg.author.id, message));
            e.set_color(15548997); // red

            dpp::embed_footer e_footer;
            e_footer.set_text(fmt::format("Powered by {}@{} - {}",
                                          m_bot->getName(), m_bot->getVersion(),
                                          m_bot->getAuthor()));
            e.set_footer(e_footer);

            dpp::message m{msg.channel_id, e};
            m.set_reference(msg.id);

            m_bot->cluster->message_create(m);
          }
        } catch (std::exception &e) {
          m_bot->cluster->log(dpp::ll_error,
                              fmt::format("error: {}", e.what()));
          continue;
        }
      }
    }
  }
}

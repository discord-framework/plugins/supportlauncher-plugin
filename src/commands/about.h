#pragma once

#include "../supportlauncher.h"
#include <string>

void about_response(const dpp::interaction_create_t &ic, Bot *bot) {
  dpp::embed e;
  e.set_title(fmt::format("Support-Launcher"));
  e.set_description(
      "Ce bot a pour but de gérer et de simplifier certains processus sur le serveur Discord de Support-Launcher.\n\
Il va téléverser automatiquement les fichiers sur Hastebin (peut être évité en écrivant `--ignore` dans le message).");
  e.set_color(3447003); // blue

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));
  e.set_footer(e_footer);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);
}

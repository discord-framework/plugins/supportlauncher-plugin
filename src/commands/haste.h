#pragma once

#include "../supportlauncher.h"
#include <string>

void haste_response(const dpp::interaction_create_t &ic, Bot *bot,
                    std::string url) {
  std::string msg = fmt::format(
      ":warning: Si ton code est trop long, ne l'envoie pas directement "
      "sur discord, passe par ce site: {}",
      url);

  auto snow = std::get_if<dpp::snowflake>(&ic.get_parameter("mention"));

  if (snow != nullptr) {
    msg = fmt::format("<@{}> !\n{}", *snow, msg);
  }

  dpp::embed e;
  e.set_title(fmt::format("Support-Launcher"));
  e.set_description(msg);
  e.set_color(3447003); // blue

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));
  e.set_footer(e_footer);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);

  ic.reply(dpp::ir_channel_message_with_source, m);
}

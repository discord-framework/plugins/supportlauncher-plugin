#pragma once

#include "../supportlauncher.h"
#include <string>

void packs_response(const dpp::interaction_create_t &ic, Bot *bot,
                    std::string url) {
  dpp::embed e;
  e.set_title(fmt::format("Packs Minecraft"));
  e.set_description(fmt::format(
      "Voici le lien vers les différents packs Minecraft disponibles: {}",
      url));
  e.set_color(3447003);

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));
  e.set_footer(e_footer);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);
}

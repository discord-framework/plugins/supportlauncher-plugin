#pragma once

#include "../supportlauncher.h"
#include <string>

void version_response(const dpp::interaction_create_t &ic, Bot *bot) {
  dpp::embed e;
  e.set_title(fmt::format("Équivalence des versions Minecraft/Java"));
  e.set_description("Chaque version de Minecraft a besoin d'une version "
                    "spécifique de Java. Voici la liste des équivalences:\n"
                    " - jusqu'à `1.12.2` (incluse) -> Java 8 (=1.8)\n"
                    " - de `1.13` à `1.16.5` vanilla -> Java 8 et 11\n"
                    " - de `1.13` à `1.16.5` forge -> Java 8\n"
                    " - `1.17` -> Java 16\n"
                    " - `1.18` -> Java 17");
  e.set_color(3447003); // blue

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));
  e.set_footer(e_footer);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);
}

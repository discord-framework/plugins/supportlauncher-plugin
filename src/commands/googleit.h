#pragma once

#include "../supportlauncher.h"
#include <string>

void googleit_response(const dpp::interaction_create_t &ic) {
  dpp::message m;
  m.channel_id = ic.command.channel_id;
  m.content = "https://tenor.com/view/"
              "edna-mode-gooble-google-gooble-it-gif-12159251";
  ic.reply(dpp::ir_channel_message_with_source, m);
}

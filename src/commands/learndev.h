#pragma once

#include "../supportlauncher.h"
#include "../utils/gumbo_extended.h"
#include <discordframework/utils/strings.h>
#include <string>
#include <vector>

void parse_list(GumboNode *node, std::vector<std::string> &list_ref) {
  GumboVector *children = &node->v.element.children;

  for (uint64_t i = 0; i < children->length; i++) {
    auto c = static_cast<GumboNode *>(children->data[i]);

    if (c->v.element.tag != GUMBO_TAG_LI)
      continue;
    auto a = get_first_child(GUMBO_TAG_A, c);

    if (!a)
      continue;

    auto text = get_text(a);
    auto attr = gumbo_get_attribute(&a->v.element.attributes, "href");
    if (!attr || text.empty())
      continue;

    list_ref.push_back(fmt::format("{}:\n {}", text, attr->value));
  }
}

std::vector<std::string> search_links(const std::string language,
                                      const std::string &html) {
  std::vector<std::string> list{};

  GumboOutput *output = gumbo_parse(html.c_str());

  auto title = get_element_by_id("awesome-learning-dev-", output->root);

  if (!title)
    return {};

  auto container = title->parent;

  if (!container)
    return {};

  GumboVector *childrens = &container->v.element.children;

  GumboNode *header3 = nullptr;
  for (uint64_t i = 0; i < childrens->length; i++) {
    auto c = static_cast<GumboNode *>(childrens->data[i]);
    if (c->v.element.tag != GUMBO_TAG_H3)
      continue;

    auto t = get_text(c);

    if (Helpers::Strings::toLower(t) == Helpers::Strings::toLower(language)) {
      header3 = c;
      break;
    }
  }

  if (!header3)
    return {};

  auto ul = get_next_element_by_tag(GUMBO_TAG_UL, header3);
  // FR list
  parse_list(ul, list);

  ul = get_next_element_by_tag(GUMBO_TAG_UL, ul);
  // EN list
  parse_list(ul, list);

  gumbo_destroy_output(&kGumboDefaultOptions, output);

  return list;
}

void learndev_response(const dpp::interaction_create_t &ic, Bot *bot,
                       const std::string &html) {

  std::string language = std::get<std::string>(ic.get_parameter("name"));

  auto links = search_links(language, html);

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));

  dpp::embed e;
  e.set_title(fmt::format("LearnDev.info"));
  e.set_url("https://www.learndev.info/fr");
  e.set_color(3447003); // blue
  e.set_footer(e_footer);

  if (links.empty()) {
    e.set_description(fmt::format(
        "Désolé, je n'ai pas réussi à trouver de cours pour \"{}\"", language));

    dpp::message m{ic.command.channel_id, e};
    m.set_flags(dpp::m_ephemeral);
    ic.reply(dpp::ir_channel_message_with_source, m);
    return;
  }

  std::string desc =
      fmt::format("**Liste de liens pour les cours de {}:**", language);

  for (auto l : links) {
    desc += fmt::format("\n\n- {}", l);
  }

  e.set_description(desc);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);
}

#pragma once

#include "../supportlauncher.h"
#include <string>

void ask_response(const dpp::interaction_create_t &ic, Bot *bot) {
  dpp::embed e;
  e.set_title(fmt::format("Don't ask to ask"));
  e.set_description(
      "Cela ne sert à rien de demander pour de l'aide. Pour bien poser une "
      "question il faut poser la question en détaillant bien le problème.\n"
      "S'il y a du code ou une erreur, il faut fournir les 2 afin que l'on "
      "puisse t'aider au mieux.\n"
      "Donc au lieu de dire:\n"
      "> J'ai besoin d'aide\n"
      "Il est préférable de dire:\n"
      "> J'ai un soucis avec *blabla...*\n"
      "> voici mon code:\n"
      "> > `code`\n"
      "> voici l'erreur\n"
      "> > `erreur`\n"
      "*Lien: https://dontasktoask.com/*");
  e.set_color(3447003); // blue

  dpp::embed_footer e_footer;
  e_footer.set_text(fmt::format("Powered by {}@{} - {}", bot->getName(),
                                bot->getVersion(), bot->getAuthor()));
  e.set_footer(e_footer);

  dpp::message m{ic.command.channel_id, e};
  ic.reply(dpp::ir_channel_message_with_source, m);
}

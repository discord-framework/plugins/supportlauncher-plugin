#pragma once

#include <discordframework/export.h>
#include <discordframework/services/service.h>

#include <spdlog/spdlog.h>

namespace DiscordFramework::Services {

class EXPORT_DF LoggerService : public Service {
public:
  /**
   * @brief Initialize the logger service
   * The logger will use an ouptput will if specified
   */
  explicit LoggerService();

  /**
   * @brief Get the logger instance
   *
   * @return spdlog::logger*
   */
  static spdlog::logger *instance();

  ~LoggerService() override = default;
};

} // namespace DiscordFramework::Services

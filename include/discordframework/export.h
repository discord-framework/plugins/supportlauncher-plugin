#pragma once

#ifdef _DOXYGEN_
#define EXPORT_DF
#else
#define EXPORT_DF __attribute__((visibility("default")))
#endif
package("args")
    set_homepage("https://github.com/Taywee/args")
    set_description("A simple header-only C++ argument parser library.")

    add_urls("https://github.com/Taywee/args.git")
    add_versions("6.3.0", "a48e1f880813b367d2354963a58dedbf2b708584")

    add_deps("cmake")

    on_install(function (package)
        local configs = {}
        table.insert(configs, "-DCMAKE_BUILD_TYPE="..(package:debug() and "Debug" or "Release"))
        table.insert(configs, "-DBUILD_SHARED_LIBS="..(package:config("shared") and "ON" or "OFF"))
        import("package.tools.cmake").install(package, configs)
    end)
